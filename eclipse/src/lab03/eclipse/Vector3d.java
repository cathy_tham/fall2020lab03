//Cathy Tham, 1944919

package lab03.eclipse;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	//constructor
	public Vector3d(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	//instance methods
	
	public double magnitude() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
	}
	
	public double dotProduct(Vector3d vector2) {
		return (x*vector2.x)+(y*vector2.y)+(z*vector2.z);
	}
	
	public Vector3d add(Vector3d vector3) {
		double addX=x+vector3.x;
		double addY=y+vector3.y;
		double addZ=z+vector3.z;
		Vector3d newVector=new Vector3d(addX,addY,addZ);
		return newVector;
	}
	

	public String toString() {
		 return ("x : "+x+ " y : " +y+ " z : "+z);
	}

}
