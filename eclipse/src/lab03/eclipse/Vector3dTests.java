//Cathy Tham, 1944919

package lab03.eclipse;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {
	@Test 
	public void testGetVector3d() {
		Vector3d testVector = new Vector3d(3, 6, 0);
		assertEquals(3, testVector.getX());
		assertEquals(6, testVector.getY());
		assertEquals(0, testVector.getZ());
	}
	
	@Test 
	public void testMagnitude() {
		Vector3d testVector = new Vector3d(3, 6, 0);
		double expectedResult=6.708203932499369;
		assertEquals(expectedResult, testVector.magnitude());
	}
	
	@Test
	public void testDotProduct() {
		Vector3d testVector1 = new Vector3d(3, 6, 1);
		Vector3d testVector2 = new Vector3d(2, 0, 7.5);
		double expectedResult=13.5;
		assertEquals(expectedResult, testVector1.dotProduct(testVector2));
	}
	
	@Test
	public void testAdd() {
		Vector3d testVector1= new Vector3d(7,1.5,9);
		Vector3d testVector2= new Vector3d(10,4,4);
		double expectedX = 17.0;
		double expectedY = 5.5;
		double expectedZ = 13.0;
		Vector3d actualVector = testVector1.add(testVector2);
		assertEquals(expectedX,actualVector.getX());
		assertEquals(expectedY,actualVector.getY());
		assertEquals(expectedZ,actualVector.getZ());
	}
}
	
